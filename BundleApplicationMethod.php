<?php namespace Decoupled\Core\Extension\Bundle;

use Decoupled\Core\Application\ApplicationContainer;
use Decoupled\Core\Bundle\BundleInterface;
use Decoupled\Core\Bundle\BundleProcessInterface;

class BundleApplicationMethod{

    public function __construct( ApplicationContainer $app )
    {
        $this->app = $app;
    }

    public function process()
    {
        $params = func_get_args();
        
        if(empty($params)) return $this->app['$bundle.initializer'];

        return $this->addProcess( $params );   
    }

    public function bundle()
    {
        $params = func_get_args();

        //if nothing passed, return bundle collection

        if(!count($params)) return $this->app['$bundle.collection'];

        //if bundle interface is passed, call add method on 
        //bundle collection

        if( $params[0] instanceof BundleInterface )
        {
            return $this->addBundle( $params );
        }

        //otherwise, assume string, and return bundle
        
        return $this->getBundle( $params );

    }

    public function addBundle( array $params )
    {
        return call_user_func_array(
            [ $this->app['$bundle.collection'], 'add' ],
            $params
        );
    }

    public function addProcess( array $params )
    {
        return call_user_func_array(
            [ $this->app['$bundle.initializer'], 'uses' ],
            $params
        );
    }

    public function getBundle( array $params )
    {
        return call_user_func_array(
            [ $this->app['$bundle.collection'], 'get' ], 
            $params
        );
    }
}