<?php

require('../vendor/autoload.php');

use phpunit\framework\TestCase;
use Decoupled\Core\Application\Application;
use Decoupled\Core\Action\ActionFactory;
use Decoupled\Core\Action\ActionInvoker;
use Decoupled\Core\Application\ApplicationContainer;
use Decoupled\Core\Extension\Bundle\BundleExtension;
use Decoupled\Core\Bundle\Test\Bundle\AppBundle;
use Decoupled\Core\Bundle\Test\Process\RoutingProcess;

class ExtTest extends TestCase{

    public function testCanUseBundleExtension()
    {
        $app = new Application( new ApplicationContainer() );

        $app->uses( new BundleExtension() );

        $this->assertInstanceOf( 'Decoupled\Core\Bundle\BundleCollectionInterface', $app->bundle() );

        $app->uses( new AppBundle );

        $this->assertNotEmpty( $app->bundle()->all() );

        $app->bundle()->remove( 'app.bundle' );

        return $app;
    }

    /**
     * @depends testCanUseBundleExtension
     */

    public function testAppBundleMethod( $app )
    {
        $app->bundle( new AppBundle );

        $bundles = $app['$bundle.collection'];

        $this->assertNotEmpty( $bundles->all() );

        $bundle = $app->bundle( 'app.bundle' );

        $this->assertEquals( 'app.bundle', $bundle->getName() );

        return $app;
    }

    /**
     * @depends testAppBundleMethod
     */

    public function testAppProcessmethod( $app )
    {
        $process = new RoutingProcess;

        $process->router = (object) [];

        $app->process( $process );

        $app->process()->init( $app['$bundle.collection'] );

        $this->assertEquals( $process->router->example, 1 );
    }

}