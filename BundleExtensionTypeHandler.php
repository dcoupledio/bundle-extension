<?php namespace Decoupled\Core\Extension\Bundle;

use Decoupled\Core\Application\ApplicationContainer;
use Decoupled\Core\Bundle\BundleInterface;

class BundleExtensionTypeHandler{

    /**
     * Adds Bundle to Bundle Collection service via $app->use( $bundle )
     *
     * @param      \Decoupled\Core\Bundle\BundleInterface            $bundle  The bundle
     * @param      \Decoupled\Core\Application\ApplicationContainer  $app     The application
     * 
     * @return     void
     */

    public function handle( BundleInterface $bundle, ApplicationContainer $app )
    {
        $app['$bundle.collection']->add( $bundle );
    }

}