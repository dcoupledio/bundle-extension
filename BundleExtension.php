<?php namespace Decoupled\Core\Extension\Bundle;

use Decoupled\Core\Application\ApplicationExtension;
use Decoupled\Core\Bundle\BundleCollection;
use Decoupled\Core\Bundle\BundleInitializer;

class BundleExtension extends ApplicationExtension{

    public function getName()
    {
        return 'bundle.extension';
    }

    public function extend()
    {
        $app = $this->getApp();

        $methods = new BundleApplicationMethod($app);

        $app['$bundle.collection'] = function(){

            return new BundleCollection();
        };

        $app['$bundle.initializer'] = function(){

            return new BundleInitializer();
        };

        $app->setMethod(
            'bundle', 
            [ $methods, 'bundle' ]
        );

        $app->setMethod(
            'process',
            [ $methods, 'process' ]
        );

        $app->addExtensionTypeHandler(
            'Decoupled\Core\Bundle\BundleInterface',  
            [ new BundleExtensionTypeHandler(), 'handle' ]
        );
    }
}